package com.example.karen.option1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import static android.content.ContentValues.TAG;

/**
 * Created by Karen on 06.02.2018.
 */

public class DrawView extends View {

    Paint p;
    Matrix matrix;
    RectF rectf;
    Path path;

    public DrawView(Context context) {
        super(context);
        p = new Paint();
        p.setStrokeWidth(10);
        p.setStyle(Paint.Style.STROKE);
        rectf = new RectF(150, 150, 300, 300);
        matrix = new Matrix();
        path = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {

        path.reset();
        path.addRect(rectf, Path.Direction.CW);
        p.setColor(Color.BLACK);
        canvas.drawPath(path, p);

        matrix.reset();
        matrix.preRotate(30);
        matrix.preTranslate(500, 00);
        path.transform(matrix);
        p.setColor(Color.BLUE);
        canvas.drawPath(path, p);
    }


}
